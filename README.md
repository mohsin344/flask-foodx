# Food-X REST API

Backend API as offline task

#### Routes implemented:

| Endpoint          | Method | Notes                                                        |
| ----------------- | ------ | ------------------------------------------------------------ |
| `/auth/v1/signup` | `PUT`  |                                                              |
| `/auth/v1/login`  | `POST` | Returns JWT token in response                                |
| `/user/v1/`       | `GET`  | Protected route. Returns all users in db. Requires JWT Token |

#### Comments:
  * I have implemented **`Session`** (session token and last seen) information as the task required but found it odd since **`Rest API are stateless in nature`**. Sorry if I misunderstood this part.
  * Also implemented **`JWT Token Validation`** just in case.
  * Haven't used any **`ORM`** but rather direct queries using **`python-mysql-connector`**.
  * Have written **`PyTest`** suite to validate the api responses. Tests are pretty basic but should suffice for now.
  * Have containerized the database **`MySQL`** just so that it is easier for the reviewer. When docker-compose command is run, it automatically initializes the database with given [Schema](db/migrations/001.sql) migration.

#### Working Proofs:
  * Postman Collection: 
  [![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/4251584-8dee9726-8b06-457e-ac5d-a3746e8f14d7?action=collection%2Ffork&collection-url=entityId%3D4251584-8dee9726-8b06-457e-ac5d-a3746e8f14d7%26entityType%3Dcollection%26workspaceId%3D01f66546-5532-4f78-8dc7-601e13be8bc0)
  
  * Video:
    * [Steps to Start Project + PyTest + Postman](videos/Steps%20to%20Start%20Project%20+%20PyTest%20+%20Postman.mov)
    * [SignUp when exists + Showing DB via Adminer](videos/SignUp%20when%20exists%20+%20Showing%20DB%20via%20Adminer.mov)

#### Steps to start project:
1. `docker-compose up`
   
2. `python src/index.py` 
   **Note:** Please let the docker image fully start before starting **Flask application**

3. `pytest` (Only for testing)

#### Database Schema:
![alt text](db/ERD/v1.png "Entity Relationship Diagram")