FROM python:3.9.5-buster
WORKDIR /code
ENV FLASK_APP=src/index.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apt-get update && apt-get install -y gcc musl-dev
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]