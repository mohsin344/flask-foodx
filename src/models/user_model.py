class UserModel:
    def __init__(self, mydb, mycursor):
        self.db = mydb
        self.cursor = mycursor

    def getAllUsers(self):
        try:
            sql = "SELECT * FROM `users`"
            self.cursor.execute(sql)
            return self.cursor.fetchall()
        except Exception as e:
            print(f"Model Error: {e}")

    def getByPublicId(self, public_id):
        try:
            sql = f"SELECT * FROM `users` WHERE public_id='{public_id}'"
            self.cursor.execute(sql)
            return self.cursor.fetchone()
        except Exception as e:
            print(f"Model Error: {e}")

    def getById(self, user_id):
        try:
            sql = f"SELECT * FROM `users` WHERE id='{user_id}'"
            self.cursor.execute(sql)
            return self.cursor.fetchone()
        except Exception as e:
            print(f"Model Error: {e}")

    def getByUsername(self, username):
        try:
            sql = f"SELECT * FROM `users` WHERE username='{username}'"
            self.cursor.execute(sql)
            return self.cursor.fetchone()
        except Exception as e:
            print(f"Model Error: {e}")

    def getByEmail(self, email):
        try:
            sql = f"SELECT * FROM `users` WHERE email='{email}'"
            self.cursor.execute(sql)
            return self.cursor.fetchone()
        except Exception as e:
            print(f"Model Error: {e}")

    def addUser(self, user):
        try:
            # Check if user already exists
            emailAlreadyExists = self.getByEmail(user["email"])
            if emailAlreadyExists:
                return {"message": "failed", "data": "Email already in use!"}

            usernameAlreadyExists = self.getByUsername(user["username"])
            if usernameAlreadyExists:
                return {"message": "failed", "data": "Username already in use!"}

            # If user doesn't exists, add new
            sql = "INSERT INTO `users` (`public_id`, `first_name`, `last_name`, `username`, `password`, `email`, `role`) VALUES (%s, %s, %s, %s, %s, %s, %s)"
            values = list(user.values())
            self.cursor.execute(sql, values)
            self.db.commit()

            return {"message": "success", "data": self.getById(self.cursor.lastrowid)}
        except Exception as e:
            return {"message": "failed", "data": e.__dict__}

    def updateSession(self, public_id, session_token=None):
        try:
            userExists = self.getByPublicId(public_id)
            if not userExists:
                return False

            # Update session information
            sql = f"UPDATE `users` SET `last_seen`=CURRENT_TIMESTAMP, `session_token`='{session_token}' WHERE `public_id` = '{public_id}'"
            self.cursor.execute(sql)
            self.db.commit()

            return True
        except Exception as e:
            print(f"Model Error: {e}")

    def deleteUserRoles(self):
        try:
            sql = f"DELETE FROM `users` WHERE `role`='user'"
            self.cursor.execute(sql)
            return {}
        except Exception as e:
            print(f"Model Error: {e}")
