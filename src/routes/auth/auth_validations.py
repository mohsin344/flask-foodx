from flask_restful import reqparse
from werkzeug.routing import ValidationError
from email.utils import parseaddr

# Custom validators


def email():
    def validate(s):
        if parseaddr(s):
            return s
        raise ValidationError(f"Email is invalid")
    return validate


def min_length(min_length):
    def validate(s):
        if len(s) >= min_length:
            return s
        raise ValidationError(
            f"String must be at least {min_length} characters long")
    return validate


def max_length(max_length):
    def validate(s):
        if len(s) <= max_length:
            return s
        raise ValidationError(
            f"String must cannot exceed {max_length} characters long")
    return validate


# Sign Up args

sign_up_args = reqparse.RequestParser()

sign_up_args.add_argument(
    "first_name", type=min_length(2), required=True)
sign_up_args.add_argument(
    "last_name", type=min_length(2), required=True)
sign_up_args.add_argument(
    "username", type=min_length(4), required=True)
sign_up_args.add_argument(
    "password", type=min_length(4), required=True)
sign_up_args.add_argument(
    "email", type=email(), required=True)
sign_up_args.add_argument(
    "role", type=str, help="Role can only be 'admin' or 'user'", required=True, choices=('admin', 'user'))

# Log In args

log_in_args = reqparse.RequestParser()

log_in_args.add_argument(
    "username", type=min_length(4), required=True)
log_in_args.add_argument(
    "password", type=min_length(4), required=True)
