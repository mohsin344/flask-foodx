from flask import Blueprint, request
from flask_restful import Api, Resource, abort
from werkzeug.security import generate_password_hash, check_password_hash
from dotenv import load_dotenv
from os import getenv
import json
import mysql.connector
import uuid
import jwt
import datetime

from .auth_validations import *
from models.user_model import UserModel

load_dotenv()

mydb = mysql.connector.connect(
    host=getenv("DB_HOSTNAME"),
    user=getenv("DB_USERNAME"),
    password=getenv("DB_PASSWORD"),
    database=getenv("DB_DATABASE"),
    port=getenv("DB_PORT")
)
mycursor = mydb.cursor(dictionary=True)

auth = Blueprint("auth", __name__, url_prefix="/auth")
api = Api(auth)
db = UserModel(mydb, mycursor)


class SignUp(Resource):
    def put(self):
        args = sign_up_args.parse_args()
        hashedPassword = generate_password_hash(
            args["password"], method='sha256')

        newUser = {
            "public_id": str(uuid.uuid4()),
            "first_name": args['first_name'],
            "last_name": args['last_name'],
            "username": args['username'],
            "password": hashedPassword,
            "email": args['email'],
            "role": args['role'],
        }

        # Insert user to database
        response = db.addUser(newUser)
        if response["message"] != "success":
            return response, 400

        # Not want to show hashed password to the client side
        del newUser["password"]

        # Respond with the details of the newly created User
        return json.dumps({
            "message": "success",
            "data": newUser
        }), 201


class LogIn(Resource):
    def post(self):
        args = log_in_args.parse_args()

        user = db.getByUsername(args["username"])
        if not user:
            abort(400, message="Invalid username or password!")

        validPass = check_password_hash(user["password"], args["password"])
        if not validPass:
            abort(400, message="Invalid username or password!")

        token = jwt.encode(
            {'id': user["public_id"], "iat": datetime.datetime.utcnow(
            ), "exp": datetime.datetime.utcnow() + datetime.timedelta(days=30)},
            getenv("TOKEN_SECRET"), algorithm="HS256")

        # Update session information
        db.updateSession(user["public_id"], token)

        return {"message": "success", "data": token}


api.add_resource(SignUp, '/v1/signup')
api.add_resource(LogIn, '/v1/login')
