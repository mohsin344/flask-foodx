from flask import Blueprint
from flask_restful import Api, Resource
from dotenv import load_dotenv
from os import getenv
from lib.route_guard import token_required
from models.user_model import UserModel
import mysql.connector

load_dotenv()

mydb = mysql.connector.connect(
    host=getenv("DB_HOSTNAME"),
    user=getenv("DB_USERNAME"),
    password=getenv("DB_PASSWORD"),
    database=getenv("DB_DATABASE"),
    port=getenv("DB_PORT")
)
mycursor = mydb.cursor(dictionary=True)

user = Blueprint("user", __name__, url_prefix="/user")
api = Api(user)
db = UserModel(mydb, mycursor)


class User(Resource):
    method_decorators = [token_required]

    def get(self, current_user):
        dbUsers = db.getAllUsers()
        users = []

        for user in dbUsers:
            user_data = {}
            user_data["public_id"] = user["public_id"]
            user_data["first_name"] = user["first_name"]
            user_data["last_name"] = user["last_name"]
            user_data["username"] = user["username"]
            user_data["password"] = user["password"]
            user_data["email"] = user["email"]
            user_data["session_token"] = user["session_token"]
            user_data["last_seen"] = user["last_seen"].strftime(
                "%Y-%m-%d %H:%M:%S") if user["last_seen"] else user["last_seen"]
            users.append(user_data)

        return {"message": "success", "data": users}


api.add_resource(User, '/v1/')
