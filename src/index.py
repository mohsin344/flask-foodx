from flask import Flask
from flask_restful import Api, Resource, reqparse, abort

from routes.auth.auth_routes import auth
from routes.user.user_routes import user

app = Flask(__name__)
api = Api(app)

app.register_blueprint(user)
app.register_blueprint(auth)

if __name__ == "__main__":
    app.run(debug=True)
