from models.user_model import UserModel
from dotenv import load_dotenv
from os import getenv
import pytest
import requests
import mysql.connector

load_dotenv()

mydb = mysql.connector.connect(
    host=getenv("DB_HOSTNAME"),
    user=getenv("DB_USERNAME"),
    password=getenv("DB_PASSWORD"),
    database=getenv("DB_DATABASE"),
    port=getenv("DB_PORT")
)
mycursor = mydb.cursor(dictionary=True)

Base_URL = "http://127.0.0.1:5000"
db = UserModel(mydb, mycursor)

signup_data = {
    "email_missing": {"first_name": "John", "last_name": "Doe",
                      "username": "test2", "password": "user", "role": "user"},
    "role_missing": {"first_name": "John", "last_name": "Doe",
                     "username": "test3", "password": "user", "email": "test3@gmail.com"},
    "invalid_email": {"first_name": "John", "last_name": "Doe",
                      "username": "test4", "password": "user", "email": "test4", "role": "user"},
}

login_data = {
    "wrong_username": {"username": "dummy", "password": "admin"},
    "wrong_password": {"username": "admin", "password": "1234"},
    "missing_username": {"password": "1234"},
    "missing_password": {"username": "dummy"},
}


@pytest.fixture
def cleanup():
    try:
        # Clear users from db to make things easier for now
        db.deleteUserRoles()
        yield
    except:
        assert False


@pytest.mark.usefixtures("cleanup")
class TestSignUp:
    def test_signup_email_missing(self):
        try:
            # Make request
            response = requests.put(
                f"{Base_URL}/auth/v1/signup", json=signup_data["email_missing"])
            data = response.json()

            assert response.status_code == 400
            assert data["message"] == {
                'email': 'Missing required parameter in the JSON body or the post body or the query string'}
        except Exception as e:
            print(e)
            assert False

    def test_signup_role_missing(self):
        try:
            # Make request
            response = requests.put(
                f"{Base_URL}/auth/v1/signup", json=signup_data["role_missing"])
            data = response.json()

            assert response.status_code == 400
            assert data["message"] == {
                'role': "Role can only be 'admin' or 'user'"}
        except Exception as e:
            print(e)
            assert False

    def test_signup_invalid_email(self):
        try:
            # Make request
            response = requests.put(
                f"{Base_URL}/auth/v1/signup", json=signup_data["invalid_email"])
            data = response.json()

            assert response.status_code == 400
        except Exception as e:
            print(e)
            assert False


@pytest.mark.usefixtures("cleanup")
class TestLogin:
    def test_login_wrong_username(self):
        try:
            # Make request
            response = requests.post(
                f"{Base_URL}/auth/v1/login", json=login_data["wrong_username"])
            data = response.json()

            assert response.status_code == 400
            assert data["message"] == "Invalid username or password!"
        except Exception as e:
            print(e)
            assert False

    def test_login_wrong_password(self):
        try:
            # Make request
            response = requests.post(
                f"{Base_URL}/auth/v1/login", json=login_data["wrong_password"])
            data = response.json()

            assert response.status_code == 400
            assert data["message"] == "Invalid username or password!"
        except Exception as e:
            print(e)
            assert False

    def test_login_missing_username(self):
        try:
            # Make request
            response = requests.post(
                f"{Base_URL}/auth/v1/login", json=login_data["missing_username"])
            data = response.json()

            assert response.status_code == 400
            assert data["message"] == {
                'username': 'Missing required parameter in the JSON body or the post body or the query string'}
        except Exception as e:
            print(e)
            assert False

    def test_login_missing_password(self):
        try:
            # Make request
            response = requests.post(
                f"{Base_URL}/auth/v1/login", json=login_data["missing_password"])
            data = response.json()

            assert response.status_code == 400
            assert data["message"] == {
                'password': 'Missing required parameter in the JSON body or the post body or the query string'}
        except Exception as e:
            print(e)
            assert False
