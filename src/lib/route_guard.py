from flask import request
from flask_restful import abort
from dotenv import load_dotenv
from os import getenv
from models.user_model import UserModel
from functools import wraps
import mysql.connector
import jwt

load_dotenv()

mydb = mysql.connector.connect(
    host=getenv("DB_HOSTNAME"),
    user=getenv("DB_USERNAME"),
    password=getenv("DB_PASSWORD"),
    database=getenv("DB_DATABASE"),
    port=getenv("DB_PORT")
)
mycursor = mydb.cursor(dictionary=True)

db = UserModel(mydb, mycursor)


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'X-Access-Token' in request.headers:
            token = request.headers['X-Access-Token']

        if not token:
            abort(401, message="Token is missing!")

        try:
            data = jwt.decode(token, getenv(
                "TOKEN_SECRET"), algorithms="HS256")
            current_user = db.getByPublicId(data['id'])
        except Exception as e:
            abort(401, message="Token is invalid!")

        return f(current_user, *args, **kwargs)

    return decorated
